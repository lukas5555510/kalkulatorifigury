﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    public class Kwadrat : IOblicz
    {        
        public float obwod(float a, float b, float h)
        {
            return 4*a;
        }

        public float pole(float a, float b, float h)
        {
            return a * a;
        }
    }
}
