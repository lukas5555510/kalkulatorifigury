﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            IOblicz k;
            float a = 0;
            float b = 0;
            float h = 0;

            k = new Kwadrat();
            Console.WriteLine("kwadrat");
            a = 5;
            Console.WriteLine(k.obwod(a, 0, 0));
            Console.WriteLine(k.pole(a, 0, 0));

            k = new Kolo();
            Console.WriteLine("koło");
            a = 10;
            Console.WriteLine(k.obwod(a, 0, 0));
            Console.WriteLine(k.pole(a, 0, 0));

            k = new Prostokat();
            Console.WriteLine("kwadrat");
            a = 2;
            b = 10;
            Console.WriteLine(k.obwod(a, b, 0));
            Console.WriteLine(k.pole(a, b, 0));

            k = new Trapez();
            Console.WriteLine("trapez");
            a = 1;
            b = 2;
            h = 3;
            Console.WriteLine(k.obwod(a, b, h));
            Console.WriteLine(k.pole(a, b, h));

            Console.Read();
        }
    }
}
