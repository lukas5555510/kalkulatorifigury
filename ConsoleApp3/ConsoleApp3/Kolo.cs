﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    public class Kolo :IOblicz
    {

        
        public float obwod(float a, float b, float h)
        {
            return 2 * a * 3.14f;
        }

        public float pole(float a, float b, float h)
        {
            return 3.14f * a * a;
        }
    }
}
