﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    interface IOblicz
    {
        float pole(float a ,float b, float h);
        float obwod(float a, float b, float h);
    }
}
