﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    public class Trapez :IOblicz
    {
        float wynik = 0;
        
        public float obwod(float a, float b, float h)
        {
            if (a>b)
            {
                wynik = (((a - b) / 2) * ((a - b) / 2) + h * h) / (((a - b) / 2) * ((a - b) / 2) + h * h) + b + a;
            }
            if (b>a)
            {
                wynik = (((b - a) / 2) * ((b - a) / 2) + h * h) / (((b - a) / 2) * ((b - a) / 2) + h * h) + b + a;
            }
            return wynik;
        }

        public float pole(float a, float b, float h)
        {
            return (a + b / 2) * h;
        }
    }
}
