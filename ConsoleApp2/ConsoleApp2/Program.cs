﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            


                float a = float.Parse(Console.ReadLine());
                float b = float.Parse(Console.ReadLine());

                Console.WriteLine("1:+  2:-  3:*  4:/");

                int odp = int.Parse(Console.ReadLine());

                IOblicz dzialanie;
                switch (odp)
                {
                    case 1: dzialanie = new Dodawanie(); break;
                    case 2: dzialanie = new Odejmowanie(); break;
                    case 3: dzialanie = new Mnożenie(); break;
                    case 4: dzialanie = new Dzielenie(); break;
                    default: dzialanie = new Dodawanie(); break;
                }

                Console.WriteLine(dzialanie.Oblicz(a, b));

                Console.Read();
            
        }
    }
}
